#importation des librairies
import radio
import math
from microbit import *

#allumage de la radio et changement de channel
radio.on()
radio.config(channel=69)

#fonctione de desactivation
def disarmed():
    display.clear()
    display.show(Image.YES)
    # display.set_pixel(0, 3, 9)
    # display.set_pixel(1, 4, 9)
    # display.set_pixel(2, 3, 9)
    # display.set_pixel(3, 2, 9)
    # display.set_pixel(4, 1, 9)
#fonction d'activation
def armed(status):
    display.clear()
    display.show(Image.NO)
    moving(status)
    # display.set_pixel(0, 0, 9)
    # display.set_pixel(1, 1, 9)
    # display.set_pixel(2, 2, 9)
    # display.set_pixel(3, 3, 9)
    # display.set_pixel(4, 4, 9)
    # display.set_pixel(4, 0, 9)
    # display.set_pixel(3, 1, 9)
    # display.set_pixel(2, 2, 9)
    # display.set_pixel(1, 3, 9)
    # display.set_pixel(0, 4, 9)
#fonction de detection du changement de mouvements
def moving(status):
    while (radio.receive() != 'OpenZeGate'):
        radio.send("LetThemIn")
        display.clear()
        x = accelerometer.get_x()
        y = accelerometer.get_y()
        z = accelerometer.get_z()
        accel = math.sqrt(x**2 + y**2 + z**2)
        accel = round(accel)

        if accel not in range(1000-400, 1000+400):
            display.show(Image.SQUARE)
            sleep(500)
            #while radio.receive() != "elpipi":
            radio.send("aled")

def setup():
    pass

def loop():
    truc = None
    status = False
    while True:
        incoming = radio.receive()
        if incoming == 'CloseZeGate' or incoming == 'OpenZeGate':
            radio.send("LetThemIn")
            truc = incoming
            #ativation de l'alarme
            if truc == 'CloseZeGate':
                radio.send("LetThemIn")
                status = False
                armed(status)
            #desactivation de l'alarme
            elif truc == 'OpenZeGate':
                radio.send("LetThemIn")
                status = True
                disarmed()


#Appel des fonctions
setup()
loop()