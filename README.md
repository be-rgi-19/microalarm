## Répartition des tâches du projet :

- Lucas (Console MDP) :
    - Désarmement de l'alarme via mdp
    - Déclenchement de l'alarme si mot de passe erroné 3 fois de suite
    - Possibilité de changer le mot de passe
        - Par défaut : 101010
        - Code de changement : 111110
        - Mot de passe à 6 chiffres


- Alexandre (porte) : 
    - Détection des mouvements de la porte
    - Transmission de l'état de la porte à la centrale
    - Affiche l'état de l'alarme sur l'écran


- Loann (Télécommande admin) :
    - Télécommande à distance permettant d'amorcer et de désamorcer l'alarme.
    - Affiche l'état de l'alarme sur l'écran
    - Affichage permettant en cas de non-réponse de la centralisation.


- Maxime (Centrale): 
    - Gestion du statut d'activation de l'alarme par :
        Reception d'un ordre d'amorçage ou de désamorçage par la télécommande ou le digicode
        Amorce ou désamorce la détection de mouvement du détecteur de la porte

## Mise en place du projet :

- <ins>Console MDP</ins> :
    - Installer du coté exterieur de la porte
- <ins>Porte</ins> :
    - Installer sur la porte
- <ins>Télécommande admin</ins> :
    - A donner à un administrateur / gardien 
- <ins>Centrale</ins> :
    - A mettre à proximiter de la porte
        
## Matériel nécessaire :
- 4 micro:bits
- 1 sirène connectée à la centrale
