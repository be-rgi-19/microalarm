import radio
from microbit import *

# Active la radio de la Micro:bit et configure le channel d'écoute
radio.on()
radio.config(channel=69)


# Fonction pour amorcer l'alarme
def armed(incoming):
    while incoming != "LetThemIn":
        radio.send("CloseZeGate")
        radio.send("1")
        display.show(Image.NO)
        incoming = radio.receive()
    sleep(1000)
    display.clear()


# Fonction pour désamorcer l'alarme
def disarmed(incoming):
    while incoming != "LetThemIn":
        radio.send("2")
        radio.send("OpenZeGate")
        display.show(Image.YES)
        incoming = radio.receive()
    sleep(1000)
    display.clear()


# Fonctionne pour faire sonner l'alarme
def hodor():
    pin0.write_digital(1)
    sleep(1000)
    pin0.write_digital(0)


def setup():
    pass


# Boucle d'intéraction par radio avec les autres composants
def loop():
    while True:
        incoming = radio.receive()
        if incoming == "OK" or incoming == "OFF":
            disarmed(incoming)
        elif incoming == "ON":
            armed(incoming)
        elif incoming == "aled":
            radio.send("elpipi")
            hodor()


setup()
loop()
