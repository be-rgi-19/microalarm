import radio
from microbit import *

radio.on()
radio.config(channel=69)

while True:
    # Permet de vider l'interface de la microbit#
    display.clear()
    incoming = radio.receive()
    # Permet d'envoyer "ON" quand le bouton A est pressé#
    if button_a.is_pressed():
        # Tantque la valeur "1" n'est pas renvoyée, la signal est envoyé#
        while incoming != "1":
            radio.send("ON")
            incoming = radio.receive()
            # Affiche le signe "validé" sur la microbit#
            display.show(Image.YES)
            # Permet de laisser l'affichage 2sc#
        sleep(2000)

    if button_b.is_pressed():
        while incoming != "2":
            radio.send("OFF")
            incoming = radio.receive()
            display.show(Image.NO)
        sleep(2000)
